// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::git_workarea;

error_chain! {
    links {
        GitWorkarea(git_workarea::Error, git_workarea::ErrorKind)
            #[doc = "An error from the git-workarea crate."];
    }

    errors {
        /// A git error occurred.
        Git(msg: String) {
            description("git error")
            display("git error: '{}'", msg)
        }
    }
}
