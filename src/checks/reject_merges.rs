// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use impl_prelude::*;

#[derive(Debug, Default, Clone, Copy)]
/// A check which denies merge commits, including octopus merges.
pub struct RejectMerges;

impl RejectMerges {
    /// Create a new check to reject merge commits.
    pub fn new() -> Self {
        Self {}
    }
}

impl Check for RejectMerges {
    fn name(&self) -> &str {
        "reject-merges"
    }

    fn check(&self, _: &CheckGitContext, commit: &Commit) -> Result<CheckResult> {
        let mut result = CheckResult::new();

        if commit.parents.len() > 1 {
            result.add_error(format!("commit {} not allowed; it is a merge commit.", commit.sha1));
        }

        Ok(result)
    }
}

#[cfg(test)]
mod tests {
    use checks::RejectMerges;
    use checks::test::*;

    const NO_MERGES_TOPIC: &str = "2fab950c4ace0584760e00caae1bb7913b07494e";
    const WITH_MERGES_TOPIC: &str = "92f545fa6c9326fe11dfb69c3ee01af285b595f4";
    const OCTOPUS_MERGES_TOPIC: &str = "4447a8eddbccac61daa6b55642e46156011d36cb";

    #[test]
    fn test_reject_merges_no_merges() {
        let check = RejectMerges::new();
        run_check_ok("test_reject_merges_no_merges", NO_MERGES_TOPIC, check);
    }

    #[test]
    fn test_reject_merges_with_merges() {
        let check = RejectMerges::new();
        let result = run_check("test_reject_merges_with_merges", WITH_MERGES_TOPIC, check);
        test_result_errors(result, &[
            "commit 92f545fa6c9326fe11dfb69c3ee01af285b595f4 not allowed; it is a merge commit.",
        ]);
    }

    #[test]
    fn test_reject_merges_octopus_merges() {
        let check = RejectMerges::new();
        let result = run_check("test_reject_merges_no_merges", OCTOPUS_MERGES_TOPIC, check);
        test_result_errors(result, &[
            "commit 4447a8eddbccac61daa6b55642e46156011d36cb not allowed; it is a merge commit.",
        ]);
    }
}
