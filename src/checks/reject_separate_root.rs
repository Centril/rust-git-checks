// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use impl_prelude::*;

#[derive(Debug, Default, Clone, Copy)]
/// A check which denies root commits.
pub struct RejectSeparateRoot;

impl RejectSeparateRoot {
    /// Create a new check to reject new root commits.
    pub fn new() -> Self {
        Self {}
    }
}

impl Check for RejectSeparateRoot {
    fn name(&self) -> &str {
        "reject-separate-root"
    }

    fn check(&self, _: &CheckGitContext, commit: &Commit) -> Result<CheckResult> {
        let mut result = CheckResult::new();

        if commit.parents.is_empty() {
            result.add_error(format!("commit {} not allowed; it is a root commit.", commit.sha1));
        }

        Ok(result)
    }
}

#[cfg(test)]
mod tests {
    use checks::RejectSeparateRoot;
    use checks::test::*;

    const NO_ROOT_TOPIC: &str = "ba3dc3cb09a558c88282742413a2dccb17d444fc";
    const WITH_ROOT_TOPIC: &str = "ff560e8798ef7a9d10bf43660695f7155b49b398";

    #[test]
    fn test_reject_separate_root_no_root() {
        let check = RejectSeparateRoot::new();
        run_check_ok("test_reject_separate_root_no_root", NO_ROOT_TOPIC, check);
    }

    #[test]
    fn test_reject_separate_root_with_root() {
        let check = RejectSeparateRoot::new();
        let result = run_check("test_reject_separate_root_with_root",
                               WITH_ROOT_TOPIC,
                               check);
        test_result_errors(result, &[
            "commit ff560e8798ef7a9d10bf43660695f7155b49b398 not allowed; it is a root commit.",
        ]);
    }
}
