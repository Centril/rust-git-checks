// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use impl_prelude::*;

#[derive(Debug)]
/// A check which denies commits which modify files underneath certain path.
pub struct RestrictedPath {
    /// The path which may not be edited.
    path: String,
    /// Whether the check is an error or a warning.
    required: bool,
}

impl RestrictedPath {
    /// Create a check which rejects modifications to files underneath a of given path.
    pub fn new<S: ToString>(path: S) -> Self {
        Self {
            path: path.to_string(),
            required: true,
        }
    }

    /// Whether commits changing the path are errors or warnings.
    pub fn required(&mut self, required: bool) -> &mut Self {
        self.required = required;
        self
    }
}

impl ContentCheck for RestrictedPath {
    fn name(&self) -> &str {
        "restricted-path"
    }

    fn check(&self, _: &CheckGitContext, content: &Content) -> Result<CheckResult> {
        let mut result = CheckResult::new();

        let is_restricted = content.diffs()
            .iter()
            .map(|diff| diff.name.as_path())
            .any(|path| path.starts_with(&self.path));

        if is_restricted {
            if self.required {
                result.add_error(format!("{}the `{}` path is restricted.",
                                         commit_prefix_str(content, "not allowed;"),
                                         self.path));
            } else {
                result.add_warning(format!("{}the `{}` path is restricted.",
                                           commit_prefix_str(content, "should be inspected;"),
                                           self.path));
            };
        }

        Ok(result)
    }
}

#[cfg(test)]
mod tests {
    use checks::RestrictedPath;
    use checks::test::*;

    const BAD_TOPIC: &str = "e845fa2521c17bdd31d5891c1c644fb17f0629db";
    const FIX_TOPIC: &str = "d8a2f22943cdcca373f00892a23b85f3a6ba1196";

    #[test]
    fn test_restricted_path() {
        let check = RestrictedPath::new("restricted");
        let result = run_check("test_restricted_path", BAD_TOPIC, check);
        test_result_errors(result, &[
            "commit e845fa2521c17bdd31d5891c1c644fb17f0629db not allowed; the `restricted` path \
             is restricted.",
        ]);
    }

    #[test]
    fn test_restricted_path_topic() {
        let check = RestrictedPath::new("restricted");
        let result = run_topic_check("test_restricted_path_topic", BAD_TOPIC, check);
        test_result_errors(result, &[
            "the `restricted` path is restricted.",
        ]);
    }

    #[test]
    fn test_restricted_path_warning() {
        let mut check = RestrictedPath::new("restricted");
        check.required(false);
        let result = run_check("test_restricted_path_warning", BAD_TOPIC, check);
        test_result_warnings(result, &[
            "commit e845fa2521c17bdd31d5891c1c644fb17f0629db should be inspected; the \
             `restricted` path is restricted.",
        ]);
    }

    #[test]
    fn test_restricted_path_warning_topic() {
        let mut check = RestrictedPath::new("restricted");
        check.required(false);
        let result = run_topic_check("test_restricted_path_warning_topic", BAD_TOPIC, check);
        test_result_warnings(result, &[
            "the `restricted` path is restricted.",
        ]);
    }

    #[test]
    fn test_restricted_path_topic_fixed() {
        let check = RestrictedPath::new("restricted");
        run_topic_check_ok("test_restricted_path_topic_fixed", FIX_TOPIC, check);
    }
}
