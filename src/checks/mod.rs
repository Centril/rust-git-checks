// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Basic Checks
//!
//! Simple checks useful in a wide variety of projects.

mod allow_robot;
pub use self::allow_robot::AllowRobot;

mod bad_commits;
pub use self::bad_commits::BadCommits;

mod changelog;
pub use self::changelog::Changelog;
pub use self::changelog::ChangelogStyle;

mod commit_subject;
pub use self::commit_subject::CommitSubject;

mod check_end_of_line;
pub use self::check_end_of_line::CheckEndOfLine;

mod check_executable_permissions;
pub use self::check_executable_permissions::CheckExecutablePermissions;

mod check_size;
pub use self::check_size::CheckSize;

mod check_whitespace;
pub use self::check_whitespace::CheckWhitespace;

mod formatting;
pub use self::formatting::Formatting;

mod invalid_paths;
pub use self::invalid_paths::InvalidPaths;

mod invalid_utf8;
pub use self::invalid_utf8::InvalidUtf8;

mod lfs_pointer;
pub use self::lfs_pointer::LfsPointer;

mod reject_conflict_paths;
pub use self::reject_conflict_paths::RejectConflictPaths;

mod reject_merges;
pub use self::reject_merges::RejectMerges;

mod reject_separate_root;
pub use self::reject_separate_root::RejectSeparateRoot;

mod reject_symlinks;
pub use self::reject_symlinks::RejectSymlinks;

mod release_branch;
pub use self::release_branch::ReleaseBranch;

mod restricted_path;
pub use self::restricted_path::RestrictedPath;

mod submodule_available;
pub use self::submodule_available::SubmoduleAvailable;

mod submodule_rewind;
pub use self::submodule_rewind::SubmoduleRewind;

mod submodule_watch;
pub use self::submodule_watch::SubmoduleWatch;

mod third_party;
pub use self::third_party::ThirdParty;

mod valid_name;
pub use self::valid_name::ValidName;
pub use self::valid_name::ValidNameFullNamePolicy;

#[cfg(test)]
pub mod test;
