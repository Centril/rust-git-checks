// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use impl_prelude::*;

#[derive(Debug)]
/// A check to allow robots to skip all checks.
pub struct AllowRobot {
    /// The identity of the robot.
    identity: Identity,
}

impl AllowRobot {
    /// Create a new check to allow robots to skip checks.
    ///
    /// Any actions performed by the identity are waved through and all other checks are ignored.
    pub fn new(identity: Identity) -> Self {
        Self {
            identity: identity,
        }
    }
}

impl BranchCheck for AllowRobot {
    fn name(&self) -> &str {
        "allow-robot"
    }

    fn check(&self, ctx: &CheckGitContext, _: &CommitId) -> Result<CheckResult> {
        let mut result = CheckResult::new();

        if *ctx.topic_owner() == self.identity {
            result.whitelist();
        }

        Ok(result)
    }
}

#[cfg(test)]
mod tests {
    use crates::git_workarea::Identity;

    use checks::AllowRobot;
    use checks::test::*;

    const ALLOW_ROBOT_COMMIT: &str = "43adb8173eb6d7a39f98e1ec3351cf27414c9aa1";

    #[test]
    fn test_allow_robot_allowed() {
        let check = AllowRobot::new(Identity::new("Rust Git Checks Tests",
                                                  "rust-git-checks@example.com"));
        let result = run_branch_check("test_allow_robot_allowed", ALLOW_ROBOT_COMMIT, check);

        assert_eq!(result.warnings().len(), 0);
        assert_eq!(result.alerts().len(), 0);
        assert_eq!(result.errors().len(), 0);
        assert_eq!(result.temporary(), false);
        assert_eq!(result.allowed(), true);
        assert_eq!(result.pass(), true);
    }

    #[test]
    fn test_allow_robot_not_robot_not_whitelisted() {
        let check = AllowRobot::new(Identity::new("Rust Git Checks 7ests",
                                                  "rust-git-checks@example.com"));
        run_branch_check_ok("test_allow_robot_not_robot_not_whitelisted",
                            ALLOW_ROBOT_COMMIT,
                            check);
    }
}
