// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use impl_prelude::*;

#[derive(Debug)]
/// Check for commits which should not be in the history.
pub struct BadCommits {
    /// The set of bad commits to deny.
    bad_commits: Vec<CommitId>,
}

impl BadCommits {
    /// Create a new check which checks for and denies branches with the given bad commits.
    pub fn new<I>(bad_commits: I) -> Self
        where I: IntoIterator,
              I::Item: ToString,
    {
        Self {
            bad_commits: bad_commits.into_iter()
                .map(|s| CommitId::new(s.to_string()))
                .collect(),
        }
    }
}

impl Check for BadCommits {
    fn name(&self) -> &str {
        "bad-commits"
    }

    fn check(&self, _: &CheckGitContext, commit: &Commit) -> Result<CheckResult> {
        let mut result = CheckResult::new();

        if self.bad_commits.contains(&commit.sha1) {
            result.add_error(format!("commit {} is a known-bad commit that was removed from the \
                                      server.",
                                     commit.sha1))
                .add_alert(format!("commit {} was pushed to the server.", commit.sha1),
                           true);
        }

        Ok(result)
    }
}

#[cfg(test)]
mod tests {
    use checks::BadCommits;
    use checks::test::*;

    const GOOD_COMMIT: &str = "7b0c51ed98a23a32718ed7014d6d4a813423f1bd";
    const BAD_COMMIT: &str = "029a00428913ee915ce5ee7250c023abfbc2aca3";
    const BAD_TOPIC: &str = "3d535904b40868dcba6465cf2c3ce4358501880a";

    #[test]
    fn test_bad_commits_good_commit() {
        let check = BadCommits::new(&[
            BAD_COMMIT,
        ]);
        run_check_ok("test_bad_commits_good_commit", GOOD_COMMIT, check);
    }

    #[test]
    fn test_bad_commits_no_bad_commit() {
        let check = BadCommits::new(&[
            // This commit should never exist.
            "0000000000000000000000000000000000000000",
        ]);
        run_check_ok("test_bad_commits_no_bad_commit", BAD_TOPIC, check);
    }

    #[test]
    fn test_bad_commits_already_in_history() {
        let check = BadCommits::new(&[
            // This commit is in the shared history.
            FILLER_COMMIT,
        ]);
        run_check_ok("test_bad_commits_already_in_history", BAD_TOPIC, check);
    }

    #[test]
    fn test_bad_commits_not_already_in_history() {
        let check = BadCommits::new(&[
            // This commit is on the branch being brought in.
            BAD_COMMIT,
        ]);
        let result = run_check("test_bad_commits_not_already_in_history", BAD_TOPIC, check);

        assert_eq!(result.warnings().len(), 0);
        assert_eq!(result.alerts().len(), 1);
        assert_eq!(result.alerts()[0],
                   "commit 029a00428913ee915ce5ee7250c023abfbc2aca3 was pushed to the server.");
        assert_eq!(result.errors().len(), 1);
        assert_eq!(result.errors()[0],
                   "commit 029a00428913ee915ce5ee7250c023abfbc2aca3 is a known-bad commit that \
                    was removed from the server.");
        assert_eq!(result.temporary(), false);
        assert_eq!(result.allowed(), false);
        assert_eq!(result.pass(), false);
    }
}
